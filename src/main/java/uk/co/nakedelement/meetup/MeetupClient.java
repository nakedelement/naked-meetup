package uk.co.nakedelement.meetup;

import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.nakedelement.meetup.callback.EventCallback;
import uk.co.nakedelement.meetup.models.Event;
import uk.co.nakedelement.meetup.models.Group;

public class MeetupClient
{
    private static final Logger LOGGER = Logger.getLogger(MeetupClient.class);
    
    private static final String MEETUP_URL = "https://api.meetup.com/";
    
    private final String key;

    public MeetupClient(String key)
    {
        this.key = key;
    }

    @SuppressWarnings("unchecked")
    public void eventsFor(Collection<String> groups, EventCallback callback)
    {        
        for (final String groupName : groups)
        {
           try
           {
                if (LOGGER.isInfoEnabled())
                    LOGGER.info(new StringBuilder("Getting: ").append(groupName).toString());
                
                final Group group = group(groupName);
                
                final URL url = new URL(new StringBuilder(MEETUP_URL).append(groupName)            
                    .append("/events?&sign=true&photo-host=public&page=20&key=").append(key).toString());                
                
                final Collection<Event> events = toArray( getFile(url).getContent(), Event.class, Collection.class); 
                for(final Event event : events)
                    callback.event(event(groupName, event.getId()).setGroup(group));
            }
            catch(Exception e)
            {
                LOGGER.warn(e);
            }
        }
    }
    
    public Event event(String group, String eventId)
    {
        try
        {
            final URL url = new URL(new StringBuilder(MEETUP_URL).append(group).append("/events/").append(eventId)            
                     .append("/?&sign=true&photo-host=public&fields=featured_photo&key=").append(key).toString());
          
            return toObject( getFile(url).getContent(), Event.class );
        }
        catch(Exception e)
        {
            LOGGER.warn(e);
            return null;
        }
    }
    
    public Group group(String groupName)
    {
        try
        {
            final URL url = new URL(new StringBuilder(MEETUP_URL).append(groupName)            
                     .append("/?key=").append(key).toString());
            
            return toObject(getFile(url).getContent() , Group.class );
        }
        catch(Exception e)
        {
            LOGGER.warn(e);
            return null;
        }
    }

    /*
     *  X-RateLimit-Limit   The maximum number of requests that can be made in a window of time
     *  X-RateLimit-Remaining   The remaining number of requests allowed in the current rate limit window
     *  X-RateLimit-Reset   The number of seconds until the current rate limit window resets
     */
    
    private static Response getFile(URL url)
    {
        try
        {
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            
            final int rateLimit = toInt(conn.getHeaderField("X-RateLimit-Limit"));            
            final int rateLimitRemaining = toInt(conn.getHeaderField("X-RateLimit-Remaining"));
            final int rateLimitReset = toInt(conn.getHeaderField("X-RateLimit-Reset"));
            
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug(new StringBuilder("The maximum number of requests that can be made in a window of time: ").append(rateLimit).toString());
                LOGGER.debug(new StringBuilder("The remaining number of requests allowed in the current rate limit window: ").append(rateLimitRemaining).append("s").toString());
                LOGGER.debug(new StringBuilder("The number of seconds until the current rate limit window reset: ").append(rateLimitReset).append("s").toString());
            }
            
            if (rateLimitRemaining < 2)
                waitFor(rateLimitReset);

            final StringWriter writer = new StringWriter();
            IOUtils.copy(conn.getInputStream(), writer, StandardCharsets.UTF_8);
            final String content = writer.toString();
                        
            return new Response(content, rateLimit, rateLimitRemaining, rateLimitReset);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private static void waitFor(int seconds)
    {
        if (seconds == 0)
            return;
        
        try
        {
            LOGGER.info(new StringBuilder("Waiting for: ").append(seconds).toString());
            Thread.sleep(seconds * 1000);
        }
        catch (InterruptedException e)
        {
            LOGGER.error(e.getMessage());
        }       
    }

    private static int toInt(String s)
    {
        if (s == null)
            return 0;
        return Integer.parseInt(s);
    }

    private static <T extends Collection<T>, U> T toArray(String src, Class<U> valueType, Class<T> collectonType)
    {
        if (src == null)
            return null;

        try
        {
            final ObjectMapper mapper = new ObjectMapper();

            return mapper.readValue(src, mapper.getTypeFactory().constructCollectionType(collectonType, valueType));
        }
        catch (IOException e)
        {        
            return null;
        }
    }
    
    private static <T> T toObject(String src, Class<T> valueType)
    {
        if (src == null)
        {
            LOGGER.warn("null source");
            return null;
        }

        try
        {
            return new ObjectMapper().readValue(src, valueType);
        } catch (IOException e)
        {
            LOGGER.warn(e);
            return null;
        }
    }

}
