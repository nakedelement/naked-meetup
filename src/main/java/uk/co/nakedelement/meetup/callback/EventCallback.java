package uk.co.nakedelement.meetup.callback;

import uk.co.nakedelement.meetup.models.Event;

public interface EventCallback
{
    void event(Event event);
}
