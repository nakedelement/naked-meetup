package uk.co.nakedelement.meetup;

public class Response
{
    private final String content;
    
    private final int rateLimit;
    
    private final int rateLimitRemaining;
    
    private final int rateLimitReset;
    
    public Response(String content, int rateLimit, int rateLimitRemaining, int rateLimitReset)
    {    
        this.content = content;
        this.rateLimit = rateLimit;
        this.rateLimitRemaining = rateLimitRemaining;
        this.rateLimitReset = rateLimitReset;
    }

    public String getContent()
    {
        return content;
    }

    public int getRateLimit()
    {
        return rateLimit;
    }

    public int getRateLimitRemaining()
    {
        return rateLimitRemaining;
    }

    public int getRateLimitReset()
    {
        return rateLimitReset;
    }
    
    

}
