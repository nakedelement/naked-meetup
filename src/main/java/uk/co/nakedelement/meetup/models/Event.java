package uk.co.nakedelement.meetup.models;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Event implements Serializable
{
    private static final long serialVersionUID = 7268909718673484544L;
    
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("group")
    private Group group;

    @JsonProperty("link")
    private String link;

    @JsonProperty("description")
    private String description;

    @JsonProperty("time")
    private long time;

    @JsonProperty("duration")
    private long duration;
    
    @JsonProperty("updated")
    private long updated;
    
    @JsonProperty("venue")
    private Venue venue;
    
    @JsonProperty("fee")
    private Fee fee;
    
    @JsonProperty("featured_photo")
    private Photo featuredPhoto;

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Group getGroup()
    {
        return group;
    }
    
    public Event setGroup(Group group)
    {
        this.group = group;
        return this;
    }

    public String getLink()
    {
        return link;
    }

    public String getDescription()
    {
        return description;
    }

    public long getTime()
    {
        return time;
    }

    public long getDuration()
    {
        return duration;
    }    

    public long getUpdated()
    {
        return updated;
    }
    
    public Date getUpdatedDate()
    {
        final Date date = new Date();
        date.setTime(updated);
        return date;
    }

    public Date getStart()
    {
        final Date start = new Date();
        start.setTime(time);
        return start;
    }

    public Date getEnd()
    {
        final Date start = new Date();
        start.setTime(getTime() + getDuration());
        return start;
    }    

    public Venue getVenue()
    {
        return venue;
    }   

    public Fee getFee()
    {
        return fee;
    }   

    public Photo getFeaturedPhoto()
    {
        return featuredPhoto;
    }

    @Override
    public String toString()
    {
        return new StringBuilder("[").append(group).append("] ").append(getName()).toString();
    }   
}
