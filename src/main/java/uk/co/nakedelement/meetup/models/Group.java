package uk.co.nakedelement.meetup.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Group implements Serializable
{
    private static final long serialVersionUID = 8877042763110230091L;
    
    @JsonProperty("id")
    private Long id;
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("link")
    private String link;
    
    @JsonProperty("group_photo")
    private Photo groupPhoto;
    
    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }   

    public String getLink()
    {
        return link;
    }   

    public Photo getGroupPhoto()
    {
        return groupPhoto;
    }

    public String groupPhotoLink()
    {
        return getGroupPhoto() != null ? getGroupPhoto().getHighresLink() : null;
    }

    @Override
    public String toString()
    {
        return getName();
    }
}
