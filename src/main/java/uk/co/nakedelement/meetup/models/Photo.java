package uk.co.nakedelement.meetup.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo implements Serializable
{
    private static final long serialVersionUID = -3951743972899303201L;
    
    @JsonProperty("highres_link")
    private String highresLink;

    public String getHighresLink()
    {
        return highresLink;
    }

    @Override
    public String toString()
    {
        return getHighresLink();
    }
}
