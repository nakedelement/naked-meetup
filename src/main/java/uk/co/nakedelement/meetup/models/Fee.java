package uk.co.nakedelement.meetup.models;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Fee implements Serializable
{
    private static final long serialVersionUID = 2551030226179264443L;

    @JsonProperty("amount")
    private BigDecimal amount;
    
    @JsonProperty("currency")
    private String currency;
    
    @JsonProperty("label")
    private String label;

    public BigDecimal getAmount()
    {
        return amount;
    }

    public String getCurrency()
    {
        return currency;
    }

    public String getLabel()
    {
        return label;
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder(getLabel()).append(": ").append(getAmount()).append(" ").append(getCurrency()).toString();
    }   

}
