package uk.co.nakedelement.meetup.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Venue implements Serializable
{
    private static final long serialVersionUID = -7662653350148032266L;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address1")
    private String address1;
    
    @JsonProperty("city")
    private String city;

    public String getName()
    {
        return name;
    }

    public String getAddress1()
    {
        return address1;
    }

    public String getCity()
    {
        return city;
    }

    @Override
    public String toString()
    {
        final StringBuilder venue = new StringBuilder(getName());
        
        if (getAddress1() != null && !getAddress1().trim().isEmpty())
            venue.append(", ").append(getAddress1());
        
        if (getCity() != null && !getCity().trim().isEmpty())
            venue.append(", ").append(getCity());
        
        return venue.toString();
    }

}
