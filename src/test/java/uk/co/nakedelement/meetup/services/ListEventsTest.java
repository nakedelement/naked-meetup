package uk.co.nakedelement.meetup.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import uk.co.nakedelement.meetup.MeetupClient;
import uk.co.nakedelement.meetup.callback.EventCallback;
import uk.co.nakedelement.meetup.models.Event;

public class ListEventsTest
{
    private MeetupClient svc;

    @Before
    public void setUp() throws Exception
    {
        try(final InputStream is = new FileInputStream(new File(System.getProperty("user.home") + "/naked-meetup.properties")))
        {
            final Properties props = new Properties();
            props.load(is);
            svc = new MeetupClient(props.getProperty("key"));
        }
        
    }

    @Test
    public void allEvents()
    {
        svc.eventsFor(groups(), new EventCallback()
        {
            @Override
            public void event(Event event)
            {
                
                if (event.getFeaturedPhoto() != null)
                  System.out.println(event.getFeaturedPhoto().getHighresLink());
                
              System.out.println(event.getGroup());
              System.out.println(event.getName());
              System.out.println(event.getStart());
              System.out.println(event.getEnd());
              System.out.println(event.getDescription());
              System.out.println(event.getLink());
              System.out.println(event.getVenue());
              System.out.println(event.getFee());
              System.out.println(event.getUpdated());
              System.out.println(event.getUpdatedDate());
              System.out.println();            
            }    
        });
    }
    
    private static Collection<String> groups()
    {
        return Arrays.asList("Norfolk-Developers-NorDev", "syncnorwich", "Hot-Source-Norwich", "digital-east","Web-Design-Kings-Lynn","ux_therapy","dotnetnorwich","Norwich-Node-User-Group","rollup","TechEast-input-output-Norfolk-group","TechEast-input-output-Suffolk-group","Ipswich-Waterfront-Innovation-Network","SearchNorwich","East-of-England-Agile-User-Group","SyncIpswich-Ipswichs-Tech-Startup-Community","Suffolk-Developers","Norwich-Marketing-Meetup","LNHS-Ladies-Of-Norwich-Hacking-Society","ORG-Norwich","Data-Science-Norfolk","Tech-Design-for-Good","Digital-iQ","PHPeast","Diss-Meteor-Meetup-on-holiday","Norwich-Hack-Makerspace-Meetup","WordPress-Norwich","NorfolkIndieGameDevelopers");
    }

}
