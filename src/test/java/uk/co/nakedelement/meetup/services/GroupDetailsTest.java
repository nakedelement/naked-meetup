package uk.co.nakedelement.meetup.services;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import uk.co.nakedelement.meetup.MeetupClient;

public class GroupDetailsTest
{
    private MeetupClient svc;

    @Before
    public void setUp() throws Exception
    {
        try(final InputStream is = new FileInputStream(new File(System.getProperty("user.home") + "/naked-meetup.properties")))
        {
            final Properties props = new Properties();
            props.load(is);
            svc = new MeetupClient(props.getProperty("key"));
        }
        
    }

    @Test
    public void test()
    {
        assertEquals("Norfolk Developers - Nor(DEV)", svc.group("Norfolk-Developers-NorDev").getName());
    }

}
